int pinM1 = 10;
int pinM2 = 9;
int pinSen1 = 8;
int pinSen2 = 7;
int pinSen3 = 6;
int pinBut1 = 5;
int pinBut2 = 4;
int pinBut3 = 3;

void motorUP()
{
  digitalWrite(pinM1, HIGH);
  digitalWrite(pinM2, LOW); 
}

void motorDO()
{
  digitalWrite(pinM1, LOW);
  digitalWrite(pinM2, HIGH);
}

void motorST()
{
  digitalWrite(pinM1, LOW);
  digitalWrite(pinM2, LOW);
}

void setup() 
{
  pinMode(pinM1, OUTPUT);
  pinMode(pinM2, OUTPUT);
  pinMode(pinSen1, INPUT);
  pinMode(pinSen2, INPUT);
  pinMode(pinSen3, INPUT);
  pinMode(pinBut1, INPUT_PULLUP);
  pinMode(pinBut2, INPUT_PULLUP);
  pinMode(pinBut3, INPUT_PULLUP);
}

void loop() 
{

  int position = 0;
  int target = 0;

  //reading sensors
  int readSen1 = digitalRead(pinSen1);
  int readSen2 = digitalRead(pinSen2);
  int readSen3 = digitalRead(pinSen3);

  if(readSen1 == 1)
  {
  int position = 1;
  }
  
  if(readSen2 == 1)
  {
  int position = 2;
  }
  
  if(readSen3 == 1)
  {
  int position = 3;
  }

  //reading buttons
  int readBut1 = digitalRead(pinBut1);
  int readBut2 = digitalRead(pinBut2);
  int readBut3 = digitalRead(pinBut3);

  if(readBut1 == 0)
  {
    target = 1;
  }

  if(readBut2 == 0)
  {
    target = 2;
  }

  if(readBut3 == 0)
  {
    target = 3;
  }

  //move the elevator
  if(position == 1 && target == 2)
  {
    motorUP();
  }

  if(position == 1 && target == 3)
  {
    motorUP();
  }

  if(position == 2 && target == 1)
  {
    motorDO();
  }

  if(position == 2 && target == 3)
  {
    motorUP();
  }

  if(position == 3 && target == 1)
  {
    motorDO();
  }

  if(position == 3 && target == 2)
  {
    motorDO();
  }

  //stops the motor
  if(position == target)
  {
    motorST(); 
  }
}
